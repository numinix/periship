<?php
/**
 * @package shippingMethod
 * @copyright Copyright 2007-2008 Numinix http://www.numinix.com
 * @copyright Portions Copyright 2003-2008 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: periship.php 4 2012-01-16 23:03:52Z numinix $
 */

class periship {

  var $code;
  var $title;
  var $description;
  var $icon;
  var $enabled;
  var $account;
  var $password;
  var $zip;

  function __construct() {
    global $order, $db;

    $this->code = 'periship';
    $this->title = defined('MODULE_SHIPPING_PERISHIP_TEXT_TITLE') ? MODULE_SHIPPING_PERISHIP_TEXT_TITLE : null;
    $this->description = defined('MODULE_SHIPPING_PERISHIP_TEXT_DESCRIPTION') ? MODULE_SHIPPING_PERISHIP_TEXT_DESCRIPTION : null;
    $this->icon = '';
    $this->sort_order = defined('MODULE_SHIPPING_PERISHIP_SORT_ORDER') ? MODULE_SHIPPING_PERISHIP_SORT_ORDER : null;
    $this->account = defined('MODULE_SHIPPING_PERISHIP_ACCOUNT') ? MODULE_SHIPPING_PERISHIP_ACCOUNT : null;
    $this->password = defined('MODULE_SHIPPING_PERISHIP_PASSWORD') ? MODULE_SHIPPING_PERISHIP_PASSWORD : null;
    $this->zip = defined('SHIPPING_ORIGIN_ZIP') ? SHIPPING_ORIGIN_ZIP : null;
    $this->signature = defined('MODULE_SHIPPING_PERISHIP_SIGNATURE') ? MODULE_SHIPPING_PERISHIP_SIGNATURE : null;
    $this->variable_surcharge = defined('MODULE_SHIPPING_PERISHIP_SURCHARGE_PERCENT') ? (int)MODULE_SHIPPING_PERISHIP_SURCHARGE_PERCENT / 100 : 0;
    $this->surcharge = defined('MODULE_SHIPPING_PERISHIP_SURCHARGE') ? (int)MODULE_SHIPPING_PERISHIP_SURCHARGE : null;
    $this->methods = explode(",", (defined('MODULE_SHIPPING_PERISHIP_METHODS') ? MODULE_SHIPPING_PERISHIP_METHODS : ''));

    $this->error = false;

    if (zen_get_shipping_enabled($this->code)) {
      $this->enabled = ((defined('MODULE_SHIPPING_PERISHIP_STATUS') && MODULE_SHIPPING_PERISHIP_STATUS == 'true') ? true : false);
    }
    if ($this->enabled && (int)MODULE_SHIPPING_PERISHIP_ZONE > 0) {
      $check_flag = false;
      $check = $db->Execute("select zone_id from " . TABLE_ZONES_TO_GEO_ZONES . " where geo_zone_id = '" . MODULE_SHIPPING_PERISHIP_ZONE . "' and zone_country_id = '" . $order->delivery['country']['id'] . "' order by zone_id");
      while (!$check->EOF) {
          if ($check->fields['zone_id'] < 1) {
              $check_flag = true;
              break;
          } elseif ($check->fields['zone_id'] == $order->delivery['zone_id']) {
              $check_flag = true;
              break;
          }
      $check->MoveNext();
      }

      if ($check_flag == false) {
          $this->enabled = false;
      }
    }
  }

  function calculation($saturday = 'Y') {
    global $order, $db, $shipping_weight, $httpClient;

    $order_total = $_SESSION['cart']->show_total();
    $state = zen_get_zone_code($order->delivery['country']['id'], $order->delivery['zone_id'], '');
    if ($state == "QC") $state = "PQ";
    $postcode = str_replace(array(' ', '-'), '', $order->delivery['postcode']);
    $request = "<PeriShipRateRequest>
                 <RequestHeader>
                   <ShipperID>" . $this->account . "</ShipperID>
                   <ShipperPassword>" . $this->password . "</ShipperPassword>
                   <ShipperZIPCode>" . $this->zip . "</ShipperZIPCode>
                 </RequestHeader>
                 <RecipientInfo>
                   <RecipientState>" . $state . "</RecipientState>
                   <RecipientZip>" . $postcode . "</RecipientZip>
                 </RecipientInfo>
                 <PackageInfo>
                   <Weight>" . $shipping_weight . "</Weight>
                   <RecipientType>C</RecipientType>";
    if ($this->signature != 'none') {
      $request .= "<SignatureType>N</SignatureType>";
    }
    if ($order_total >= 101) {
      $request .= "<DeclaredValue>" . $order_total . "</DeclaredValue>";
    }
    if ($saturday == 'Y') {
      $this->saturday = true;
      $request .= "<SaturdayDelivery>" . $saturday . "</SaturdayDelivery>";
    } else {
      $this->saturday = false;
    }
    if (zen_not_null($_SESSION['ship_date'])) {
      $request .= "<ShipDate>" . $_SESSION['ship_date'] . "</ShipDate>";
    }
    $request .="</PackageInfo>
              <ReturnType>
                <FeeDetail>S</FeeDetail>
              </ReturnType>
            </PeriShipRateRequest>";

    $url = "http://www.periship.com/invoicing/controller/PeriShip.php";

    $data = array("shipment" => $request);
    $this->HttpClient("periship.com");
    $this->post($url, $data);
    $result = $this->getContent();
    if(!$result){
        $result = $this->getError();
        //$this->error = true;
    }
    return $result;
  }


  function parseXML($result) {
    $this->XMLParser($result);
    $array = $this->getOutput();
    //print_r($array);
    //die();
    $this->error_count = (int)$array['PeriShipRateResponse']['ResponseHeader']['ErrorCount'];
    if ($this->error_count > 0) {
      $errors = $array['PeriShipRateResponse']['Errors'];
      $this->error_description = '';
      foreach($errors as $error) {
        if ($this->error_description != '') {
          $this->error_description .= ' ';
        }
        $this->error_description .= $error['ErrorDescription'];
      }
    } else {
      $this->rates = $array['PeriShipRateResponse'];
      array_shift($this->rates);
      $this->rates_count = count($rates);
    }
    if ($this->saturday && $this->rates != '') {
      $this->rates['ServiceItem']['ServiceCode'] = "0";
    }
    return $this->rates;
  }

  function quote($method = '') {
    global $order, $shipping_weight, $shipping_num_boxes, $db;

    if ($this->enabled) {

      $saturday_array = $this->parseXML($this->calculation('Y'));
      $regular_array = $this->parseXML($this->calculation('N'));
      array_shift($regular_array);
      //print_r($regular_array);
      //echo '<br/><br/>';
      //$saturday_array = $saturday_array['ServiceItem'];
      $new_array[0] = $saturday_array['ServiceItem'];
      foreach ($regular_array as $element) {
        array_push($new_array, $element);
      }
      $array = $new_array;
      //var_dump($array);
      //die();
      if (is_array($array)) {
        if ($this->error_description != '') {
          $this->quotes = array('module' => $this->title,
                                'error' => $this->error_description);
        } else {
          $this->quotes = array('id' => $this->code,
                                'module' => $this->title);
          $methods = array();

          $method_descriptions = array("0" => "Priority Overnight Saturday Service",
                                       "1" => "Priority Overnight Service",
                                       "3" => "FedEx 2Day Service",
                                       "5" => "Standard Overnight Service",
                                       "20" => "Express Saver",
                                       "90" => "FedEx Home Delivery",
                                       "92" => "FedEx Ground");


          foreach ($array as $rate) {
            if ( $method == '' || $method == $method_descriptions[$rate['ServiceCode']] ) {
              //$code = trim($rate['ServiceCode']);
              if (in_array($rate['ServiceCode'], $this->methods)) {
                $methods[] = array('id' => $method_descriptions[$rate['ServiceCode']],
                                   'title' => $method_descriptions[$rate['ServiceCode']],
                                   'cost' => ($this->surcharge + $rate['TotalFee'] + ($rate['TotalFee'] * $this->variable_surcharge)) * $shipping_num_boxes);
              }
            }
          }
          $this->quotes['methods'] = $methods;
          if ($this->tax_class > 0) {
            $this->quotes['tax'] = zen_get_tax_rate($this->tax_class, $order->delivery['country']['id'], $order->delivery['zone_id']);
          }
        }
      } else {
        $this->quotes = array('module' => $this->title,
                              'error' => MODULE_SHIPPING_PERISHIP_NO_RATES);
      }
      if (zen_not_null($this->icon)) $this->quotes['icon'] = zen_image($this->icon, $this->title);

      return $this->quotes;
    }
  }

  function check() {
      global $db;
      if (!isset($this->_check)) {
          $check_query = $db->Execute("select configuration_value from " . TABLE_CONFIGURATION . " where configuration_key = 'MODULE_SHIPPING_PERISHIP_STATUS'");
          $this->_check = $check_query->RecordCount();
      }
      return $this->_check;
  }

  function install() {
    global $db;

    $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Enable PeriShip Shipping', 'MODULE_SHIPPING_PERISHIP_STATUS', 'true', 'Do you want to offer Fedex PeriShip shipping?', '6', '1', 'zen_cfg_select_option(array(\'true\', \'false\'), ', now())");
    $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Your PeriShip Account Number', 'MODULE_SHIPPING_PERISHIP_ACCOUNT', '', 'Enter the PeriShip Account Number assigned to you, required', '6', '2', now())");
    $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Your Periship Account Password', 'MODULE_SHIPPING_PERISHIP_PASSWORD', '', 'Enter the PeriShip Account Password assigned to you, required', '6', '2', now())");
    $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Periship Methods Offered', 'MODULE_SHIPPING_PERISHIP_METHODS', '0,1,3,5,20,90,92', 'Enter the PeriShip service codes separated by commas:<br/>0 = Priority Overnight Saturday Service<br/>1 = Priority Overnight Service<br/>3 = FedEx 2Day Service<br/>5 = Standard Overnight Service<br/>20 = Express Saver<br/>90 = FedEx Home Delivery<br/>92 = FedEx Ground', '6', '2', now())");
    $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Signature Type', 'MODULE_SHIPPING_PERISHIP_SIGNATURE', 'none', 'Is a signature required?<br/>D = Direct Signature Required<br/>A = Adult Signature Required', '6', '3', 'zen_cfg_select_option(array(\'none\', \'D\', \'A\'), ', now())");
    $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('PeriShip fixed surcharge?', 'MODULE_SHIPPING_PERISHIP_SURCHARGE', '0', 'Fixed surcharge amount to add to shipping charge?', '6', '4', now())");
    $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('PeriShip variable surcharge?', 'MODULE_SHIPPING_PERISHIP_SURCHARGE_PERCENT', '0', 'Percentage surcharge amount to add to shipping charge?', '6', '4', now())");
    $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, use_function, set_function, date_added) values ('Tax Class', 'MODULE_SHIPPING_PERISHIP_TAX_CLASS', '0', 'Use the following tax class on the shipping fee.', '6', '5', 'zen_get_tax_class_title', 'zen_cfg_pull_down_tax_classes(', now())");
    $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, use_function, set_function, date_added) values ('Shipping Zone', 'MODULE_SHIPPING_PERISHIP_ZONE', '0', 'If a zone is selected, only enable this shipping method for that zone.', '6', '98', 'zen_get_zone_class_title', 'zen_cfg_pull_down_zone_classes(', now())");
    $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Sort Order', 'MODULE_SHIPPING_PERISHIP_SORT_ORDER', '0', 'Sort order of display.', '6', '99', now())");
  }

  function remove() {
    global $db;

    $db->Execute("delete from " . TABLE_CONFIGURATION . " where configuration_key in ('" . implode("', '", $this->keys()) . "')");
  }

  function keys() {
    return array('MODULE_SHIPPING_PERISHIP_STATUS','MODULE_SHIPPING_PERISHIP_ACCOUNT','MODULE_SHIPPING_PERISHIP_PASSWORD','MODULE_SHIPPING_PERISHIP_METHODS','MODULE_SHIPPING_PERISHIP_SIGNATURE','MODULE_SHIPPING_PERISHIP_SURCHARGE','MODULE_SHIPPING_PERISHIP_SURCHARGE_PERCENT','MODULE_SHIPPING_PERISHIP_TAX_CLASS','MODULE_SHIPPING_PERISHIP_ZONE','MODULE_SHIPPING_PERISHIP_SORT_ORDER');
  }

/****************************************************************************************************************/
  // Request vars
  var $host;
  var $port;
  var $path;
  var $method;
  var $postdata = '';
  var $cookies = array();
  var $referer;
  var $accept = 'text/xml,application/xml,application/xhtml+xml,text/html,text/plain,image/png,image/jpeg,image/gif,*/*';
  var $accept_encoding = 'gzip';
  var $accept_language = 'en-us';
  var $user_agent = 'Incutio HttpClient v0.9';
  // Options
  var $timeout = 20;
  var $use_gzip = true;
  var $persist_cookies = true;  // If true, received cookies are placed in the $this->cookies array ready for the next request
                                // Note: This currently ignores the cookie path (and time) completely. Time is not important,
                                //       but path could possibly lead to security problems.
  var $persist_referers = true; // For each request, sends path of last request as referer
  var $debug = false;
  var $handle_redirects = true; // Auaomtically redirect if Location or URI header is found
  var $max_redirects = 5;
  var $headers_only = false;    // If true, stops receiving once headers have been read.
  // Basic authorization variables
  var $username;
  //var $password; // already set at top of class
  // Response vars
  var $status;
  var $headers = array();
  var $content = '';
  var $errormsg;
  // Tracker variables
  var $redirect_count = 0;
  var $cookie_host = '';

  function HttpClient($host, $port=80) {
      $this->host = $host;
      $this->port = $port;
  }
  function get($path, $data = false) {
      $this->path = $path;
      $this->method = 'GET';
      if ($data) {
          $this->path .= '?'.$this->buildQueryString($data);
      }
      return $this->doRequest();
  }
  function post($path, $data) {
      $this->path = $path;
      $this->method = 'POST';
      $this->postdata = $this->buildQueryString($data);
    return $this->doRequest();
  }
  function buildQueryString($data) {
      $querystring = '';
      if (is_array($data)) {
          // Change data in to postable data
      foreach ($data as $key => $val) {
        if (is_array($val)) {
          foreach ($val as $val2) {
            $querystring .= urlencode($key).'='.urlencode($val2).'&';
          }
        } else {
          $querystring .= urlencode($key).'='.urlencode($val).'&';
        }
      }
      $querystring = substr($querystring, 0, -1); // Eliminate unnecessary &
    } else {
        $querystring = $data;
    }
    return $querystring;
  }
  function doRequest() {
      // Performs the actual HTTP request, returning true or false depending on outcome
  if (!$fp = @fsockopen($this->host, $this->port, $errno, $errstr, $this->timeout)) {
      // Set error message
          switch($errno) {
      case -3:
        $this->errormsg = 'Socket creation failed (-3)';
      case -4:
        $this->errormsg = 'DNS lookup failure (-4)';
      case -5:
        $this->errormsg = 'Connection refused or timed out (-5)';
      default:
        $this->errormsg = 'Connection failed ('.$errno.')';
        $this->errormsg .= ' '.$errstr;
        $this->debug($this->errormsg);
    }
    return false;
      }
      socket_set_timeout($fp, $this->timeout);
      $request = $this->buildRequest();
      $this->debug('Request', $request);
      fwrite($fp, $request);
    // Reset all the variables that should not persist between requests
    $this->headers = array();
    $this->content = '';
    $this->errormsg = '';
    // Set a couple of flags
    $inHeaders = true;
    $atStart = true;
    // Now start reading back the response
    while (!feof($fp)) {
        $line = fgets($fp, 4096);
        if ($atStart) {
            // Deal with first line of returned data
            $atStart = false;
           /* if (!preg_match('/HTTP\/(\\d\\.\\d)\\s*(\\d+)\\s*(.*)/', $line, $m)) {
                $this->errormsg = "Status code line invalid: ".htmlentities($line);
                $this->debug($this->errormsg);
                return false;
            }
            $http_version = $m[1]; // not used
            $this->status = $m[2];
            $status_string = $m[3]; // not used
            $this->debug(trim($line));  */
            continue;
        }
        if ($inHeaders) {
            if (trim($line) == '') {
                $inHeaders = false;
                $this->debug('Received Headers', $this->headers);
                if ($this->headers_only) {
                    break; // Skip the rest of the input
                }
                continue;
            }
            if (!preg_match('/([^:]+):\\s*(.*)/', $line, $m)) {
                // Skip to the next header
                continue;
            }
            $key = strtolower(trim($m[1]));
            $val = trim($m[2]);
            // Deal with the possibility of multiple headers of same name
            if (isset($this->headers[$key])) {
                if (is_array($this->headers[$key])) {
                    $this->headers[$key][] = $val;
                } else {
                    $this->headers[$key] = array($this->headers[$key], $val);
                }
            } else {
                $this->headers[$key] = $val;
            }
            continue;
        }
        // We're not in the headers, so append the line to the contents
        $this->content .= $line;
      }
      fclose($fp);
      // If data is compressed, uncompress it
      if (isset($this->headers['content-encoding']) && $this->headers['content-encoding'] == 'gzip') {
          $this->debug('Content is gzip encoded, unzipping it');
          $this->content = substr($this->content, 10); // See http://www.php.net/manual/en/function.gzencode.php
          $this->content = gzinflate($this->content);
      }
      // If $persist_cookies, deal with any cookies
      if ($this->persist_cookies && isset($this->headers['set-cookie']) && $this->host == $this->cookie_host) {
          $cookies = $this->headers['set-cookie'];
          if (!is_array($cookies)) {
              $cookies = array($cookies);
          }
          foreach ($cookies as $cookie) {
              if (preg_match('/([^=]+)=([^;]+);/', $cookie, $m)) {
                  $this->cookies[$m[1]] = $m[2];
              }
          }
          // Record domain of cookies for security reasons
          $this->cookie_host = $this->host;
      }
      // If $persist_referers, set the referer ready for the next request
      if ($this->persist_referers) {
          $this->debug('Persisting referer: '.$this->getRequestURL());
          $this->referer = $this->getRequestURL();
      }
      // Finally, if handle_redirects and a redirect is sent, do that
      if ($this->handle_redirects) {
          if (++$this->redirect_count >= $this->max_redirects) {
              $this->errormsg = 'Number of redirects exceeded maximum ('.$this->max_redirects.')';
              $this->debug($this->errormsg);
              $this->redirect_count = 0;
              return false;
          }
          $location = isset($this->headers['location']) ? $this->headers['location'] : '';
          $uri = isset($this->headers['uri']) ? $this->headers['uri'] : '';
          if ($location || $uri) {
              $url = parse_url($location.$uri);
              // This will FAIL if redirect is to a different site
              return $this->get($url['path']);
          }
      }
      return true;
  }
  function buildRequest() {
      $headers = array();
      $headers[] = "{$this->method} {$this->path} HTTP/1.0"; // Using 1.1 leads to all manner of problems, such as "chunked" encoding
      $headers[] = "Host: {$this->host}";
      $headers[] = "User-Agent: {$this->user_agent}";
      $headers[] = "Accept: {$this->accept}";
      if ($this->use_gzip) {
          $headers[] = "Accept-encoding: {$this->accept_encoding}";
      }
      $headers[] = "Accept-language: {$this->accept_language}";
      if ($this->referer) {
          $headers[] = "Referer: {$this->referer}";
      }
    // Cookies
    if ($this->cookies) {
        $cookie = 'Cookie: ';
        foreach ($this->cookies as $key => $value) {
            $cookie .= "$key=$value; ";
        }
        $headers[] = $cookie;
    }
    // Basic authentication
    if ($this->username && $this->password) {
        $headers[] = 'Authorization: BASIC '.base64_encode($this->username.':'.$this->password);
    }
    // If this is a POST, set the content type and length
    if ($this->postdata) {
        $headers[] = 'Content-Type: application/x-www-form-urlencoded';
        $headers[] = 'Content-Length: '.strlen($this->postdata);
    }
    $request = implode("\r\n", $headers)."\r\n\r\n".$this->postdata;
    return $request;
  }
  function getContent() {
      return $this->content;
  }
  function getHeaders() {
      return $this->headers;
  }
  function getHeader($header) {
      $header = strtolower($header);
      if (isset($this->headers[$header])) {
          return $this->headers[$header];
      } else {
          return false;
      }
  }
  function getError() {
      return $this->errormsg;
  }
  function getCookies() {
      return $this->cookies;
  }
  function getRequestURL() {
      $url = 'http://'.$this->host;
      if ($this->port != 80) {
          $url .= ':'.$this->port;
      }
      $url .= $this->path;
      return $url;
  }
  // Setter methods
  function setUserAgent($string) {
      $this->user_agent = $string;
  }
  function setAuthorization($username, $password) {
      $this->username = $username;
      //$this->password = $password;
  }
  function setCookies($array) {
      $this->cookies = $array;
  }
  // Option setting methods
  function useGzip($boolean) {
      $this->use_gzip = $boolean;
  }
  function setPersistCookies($boolean) {
      $this->persist_cookies = $boolean;
  }
  function setPersistReferers($boolean) {
      $this->persist_referers = $boolean;
  }
  function setHandleRedirects($boolean) {
      $this->handle_redirects = $boolean;
  }
  function setMaxRedirects($num) {
      $this->max_redirects = $num;
  }
  function setHeadersOnly($boolean) {
      $this->headers_only = $boolean;
  }
  function setDebug($boolean) {
      $this->debug = $boolean;
  }
  // "Quick" static methods
  function quickGet($url) {
      $bits = parse_url($url);
      $host = $bits['host'];
      $port = isset($bits['port']) ? $bits['port'] : 80;
      $path = isset($bits['path']) ? $bits['path'] : '/';
      if (isset($bits['query'])) {
          $path .= '?'.$bits['query'];
      }
      $client = new HttpClient($host, $port);
      if (!$client->get($path)) {
          return false;
      } else {
          return $client->getContent();
      }
  }
  function quickPost($url, $data) {
      $bits = parse_url($url);
      $host = $bits['host'];
      $port = isset($bits['port']) ? $bits['port'] : 80;
      $path = isset($bits['path']) ? $bits['path'] : '/';
      $client = new HttpClient($host, $port);
      if (!$client->post($path, $data)) {
          return false;
      } else {
          return $client->getContent();
      }
  }
  function debug($msg, $object = false) {
      if ($this->debug) {
          print '<div style="border: 1px solid red; padding: 0.5em; margin: 0.5em;"><strong>HttpClient Debug:</strong> '.$msg;
          if ($object) {
              ob_start();
            print_r($object);
            $content = htmlentities(ob_get_contents());
            ob_end_clean();
            print '<pre>'.$content.'</pre>';
        }
        print '</div>';
      }
  }

/****************************************************************************************************************/
  // raw xml
  var $rawXML;
  // xml parser
  var $parser = null;
  // array returned by the xml parser
  var $valueArray = array();
  var $keyArray = array();

  // arrays for dealing with duplicate keys
  var $duplicateKeys = array();

  // return data
  var $output = array();
  var $statusb;

  function XMLParser($xml){
      $this->rawXML = $xml;
      $this->parser = xml_parser_create();
      return $this->parse();
  }

  function parse(){

      $parser = $this->parser;

      xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0); // Dont mess with my cAsE sEtTings
      xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 1);     // Dont bother with empty info
      if(!xml_parse_into_struct($parser, $this->rawXML, $this->valueArray, $this->keyArray)){
          $this->statusb = 'error: '.xml_error_string(xml_get_error_code($parser)).' at line '.xml_get_current_line_number($parser);
          return false;
      }
      xml_parser_free($parser);

      $this->findDuplicateKeys();

      // tmp array used for stacking
      $stack = array();
      $increment = 0;

      foreach($this->valueArray as $val) {
          if($val['type'] == "open") {
              //if array key is duplicate then send in increment
              if(array_key_exists($val['tag'], $this->duplicateKeys)){
                  array_push($stack, $this->duplicateKeys[$val['tag']]);
                  $this->duplicateKeys[$val['tag']]++;
              }
              else{
                  // else send in tag
                  array_push($stack, $val['tag']);
              }
          } elseif($val['type'] == "close") {
              array_pop($stack);
              // reset the increment if they tag does not exists in the stack
              if(array_key_exists($val['tag'], $stack)){
                  $this->duplicateKeys[$val['tag']] = 0;
              }
          } elseif($val['type'] == "complete") {
              //if array key is duplicate then send in increment
              if(array_key_exists($val['tag'], $this->duplicateKeys)){
                  array_push($stack, $this->duplicateKeys[$val['tag']]);
                  $this->duplicateKeys[$val['tag']]++;
              }
              else{
                  // else send in tag
                  array_push($stack,  $val['tag']);
              }
              $this->setArrayValue($this->output, $stack, $val['value']);
              array_pop($stack);
          }
          $increment++;
      }

      $this->statusb = 'success: xml was parsed';
      return true;

  }

  function findDuplicateKeys(){

      for($i=0;$i < count($this->valueArray); $i++) {
          // duplicate keys are when two complete tags are side by side
          if($this->valueArray[$i]['type'] == "complete"){
              if( $i+1 < count($this->valueArray) ){
                  if($this->valueArray[$i+1]['tag'] == $this->valueArray[$i]['tag'] && $this->valueArray[$i+1]['type'] == "complete"){
                      $this->duplicateKeys[$this->valueArray[$i]['tag']] = 0;
                  }
              }
          }
          // also when a close tag is before an open tag and the tags are the same
          if($this->valueArray[$i]['type'] == "close"){
              if( $i+1 < count($this->valueArray) ){
                  if(    $this->valueArray[$i+1]['type'] == "open" && $this->valueArray[$i+1]['tag'] == $this->valueArray[$i]['tag'])
                      $this->duplicateKeys[$this->valueArray[$i]['tag']] = 0;
              }
          }

      }

  }

  function setArrayValue(&$array, $stack, $value){
      if ($stack) {
          $key = array_shift($stack);
          $this->setArrayValue($array[$key], $stack, $value);
          return $array;
      } else {
          $array = $value;
      }
  }

  function getOutput(){
      return $this->output;
  }

  function getStatus(){
      return $this->statusb;
  }
}
?>
