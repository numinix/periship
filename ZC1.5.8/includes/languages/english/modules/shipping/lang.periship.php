<?php
/**
 * @package shippingMethod
 * @copyright Copyright 2007-2008 Numinix http://www.numinix.com
 * @copyright Portions Copyright 2003-2008 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: periship.php 2 2008-06-06 07:22:45Z numinix $
 */
$define = [
'MODULE_SHIPPING_PERISHIP_TEXT_TITLE' => 'PeriShip',
'MODULE_SHIPPING_PERISHIP_TEXT_DESCRIPTION' => 'PeriShip - Express Shipping Solutions for the Perishable Foods Industry',
'MODULE_SHIPPING_PERISHIP_NO_RATES' => 'No PeriShip rates returned'
];
return $define;

?>
